/**
 * Adapted from https://github.com/reduxjs/redux/blob/master/rollup.config.js
 */

import nodeResolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs'
import includePaths from 'rollup-plugin-includepaths';
import copy from 'rollup-plugin-copy';

import pkg from './package.json';



let includePathOptions = {
  include: {},
  paths: ['src'],
  external: [],
  extensions: ['.js', '.json', '.html']
};


const defaultOutputOptions = {
  format: 'cjs',
  indent: false,
  sourcemap: false,
  exports: 'named',
}


const package_externals = (pkg) => (externals = []) => {
  return externals.concat(pkg.dependencies).concat(pkg.peerDependencies);
}




const external = package_externals(pkg)([]);


const plugins = [

  commonjs({
    include: 'node_modules/**',
  }),
  babel(),
  includePaths(includePathOptions),

];

const ReactGlobals = {
  react: "React",
  "prop-types": "PropTypes",
  "react-onclickoutside": "onClickOutside",
  "react-popper": "ReactPopper"
};

const defaultConf = {
  external,
  plugins
}




const make_pkg = (input, output, more_plugins = [], globals = {}) => {
  return Object.assign(
    {},
    defaultConf,
    {
      input,
      output: Object.assign({}, defaultOutputOptions, { file: output }),
      plugins: [...plugins, ...more_plugins],
      globals
    }
  );
}

const copy_package_json = target => copy({
  targets: [
    { src: 'package.json', dest: target }
  ]
});



export default [
  make_pkg('src/index.js', 'dist/index.js'),

]
